#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_COPY_FILES := \
	device/fsl/imx53qsb/init.rc:root/init.rc \
	device/fsl/imx53qsb/init.imx53qsb.rc:root/init.imx53qsb.rc \
	device/fsl/imx53qsb/init.imx53qsb.usb.rc:root/init.imx53qsb.usb.rc \
	device/fsl/imx53qsb/ueventd.imx53qsb.rc:root/ueventd.imx53qsb.rc \
	device/fsl/imx53qsb/gpio-keys.kl:system/usr/keylayout/gpio-keys.kl \
	frameworks/base/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
	frameworks/base/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml

PRODUCT_PACKAGES := \
	gralloc.imx53qsb \
        make_ext4fs \
	com.android.future.usb.accessory

PRODUCT_PROPERTY_OVERRIDES := \
	hwui.render_dirty_regions=false

PRODUCT_CHARACTERISTICS := nosdcard

PRODUCT_TAGS += dalvik.gc.type-precise

PRODUCT_PACKAGES += \
	librs_jni \
	com.android.future.usb.accessory

# keyboard mapping files.
PRODUCT_PACKAGES += \
	mxckpd.kcm

# Filesystem management tools
PRODUCT_PACKAGES += \
	gralloc.imx53qsb \
	make_ext4fs

